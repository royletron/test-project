import React from 'react';

import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

import Folder from '../src/components/Folder';
import File from '../src/components/File';

describe('<Folder /> component', () => {

  it('renders a folder name', () => {
    const wrapper = shallow(<Folder name='Bobby' />);
    expect(wrapper.text()).to.equal('Bobby');
  });

  it('renders on div by default', () => {
    const wrapper = shallow(<Folder name='Bobby' />);
    expect(wrapper.childAt(0)).to.have.lengthOf(1);
  });

  it('expands on click', () => {
    const wrapper = shallow(<Folder name='Bobby' />);
    wrapper.childAt(0).simulate('click');
    expect(wrapper.children().length).to.be.greaterThan(1);
  });

  it('hides a file until clicked', () => {
    const wrapper = shallow(<Folder name='Bobby'><File>one</File></Folder>);
    expect(wrapper.text()).to.not.contain('File');
  });

  it('shows a file until clicked', () => {
    const wrapper = shallow(<Folder name='Bobby'><File>one</File></Folder>);
    wrapper.childAt(0).simulate('click');
    expect(wrapper.text()).to.contain('File');
  });

});
