import React from 'react';

import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

import Hello from '../src/components/Hello';

describe('<Hello /> component', () => {

  it('renders a hello', () => {
    const wrapper = shallow(<Hello name='Bobby' />);
    expect(wrapper.text()).to.equal('Hello Bobby');
  });

  it('renders a missing name', () => {
    const wrapper = shallow(<Hello />);
    expect(wrapper.text()).to.equal('Hello no one');
  });

});
