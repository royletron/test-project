# What is this?

This project should implement a simple file tree browser showing folders and
subfolders and the files inside them. Folders should be 'collapsable' so you can
click them and they either expand of collapse dependant on whether they are
currently open. 

# Getting Started

* Check out repository
* Setup yarn `npm install -g yarn`
* Install modules `yarn install`

From here you can use two commands

## Browser Preview

Run `yarn start` and point your browser to `http://localhost:3001/`. This will
give you a preview of the `src/container/index.js` file.

## Testing

Run `yarn test` to see all tests run and the output.

# Task

What you need to do:

Get all of the tests passing by modifying `src/components/Folder` and
`src/components/Hello` (tip: start with Hello first). You can do this by
reading the test to see what they're trying to do and update the components
to function that way.

And of course feel free to style it however you think fits the usage.