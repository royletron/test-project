import React from 'react';

import styles from './style.css';

const Hello = ({name}) => <h1 className={styles.this}>Hello {name}</h1>

export default Hello;
