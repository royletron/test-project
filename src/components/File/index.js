import React from 'react';

import styles from './style.css';

const File = ({children}) => <div className={styles.this}>{children}</div>

export default File;