import React from 'react';

import styles from './style.css';

export default class Folder extends React.Component {
  render () {
    let {children} = this.props;
    return (
      <div className={styles.this}>
        {children}
      </div>
    )
  }
}