import React from 'react';
import { hot } from 'react-hot-loader';

import Hello from '../components/Hello';
import styles from './style.css';

const App = () => (
  <div className={styles.this}>
    <Hello name='Adam'/>
  </div>
)

export default hot(module)(App)

